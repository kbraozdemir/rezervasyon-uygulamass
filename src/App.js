import React, { Component } from 'react';
import Date from './components/Date';
import Hotels from './components/Hotels';
import Header from './components/Header';
import Select from './components/Select';
import './App.css';


class App extends Component {
  render() {

    return (
      <div className='App'>
        <div className='header-background'>
          <Header/>
        </div>
        <hr/>
        <div className='display-flex'>
          <Date/>
          <Date/>
          <Select/>
        </div>
        <hr/>
        <div className='hotel-flex'>
          <Hotels

            name = "Otel 1"
            price = "850"
            facilities = "Kahvaltı, Ücretsiz Otopark, Ücretsiz Wi-Fi, Ücretsiz Yüzme Havuzu"
            
          />
        

          <Hotels
          name = "Otel 2"
          price = "600"
          facilities = "Kahvaltı, Ücretsiz Otopark, Ücretsiz Wi-Fi"
        
        
          />
        

          <Hotels
          name = "Otel 3"
          price = "1500"
        
        
        
          />
        
        </div>
      </div>
      
    );
  } 
}

export default App;
