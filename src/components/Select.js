import * as React from 'react';
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

export default function BasicSelect() {
  const [room, setAge] = React.useState('');

  const handleChange = (event) => {
    setAge(event.target.value);
  };

  return (
    <Box sx={{ minWidth: 120 }}>
      <FormControl fullWidth>
        <InputLabel id="demo-simple-select-label">Oda Seçiniz</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={room}
          label="Oda Seçiniz"
          onChange={handleChange}
        >
          <MenuItem value={1-1}>1 Oda 1 Misafir</MenuItem>
          <MenuItem value={1-2}>1 Oda 2 Misafir</MenuItem>
          <MenuItem value={2-2}>2 Oda 2 Misafir</MenuItem>
          <MenuItem value={2-3}>2 Oda 3 Misafir</MenuItem>
          <MenuItem value={2-4}>2 Oda 4 Misafir</MenuItem>
        </Select>
      </FormControl>
    </Box>
  );
}
