import React from 'react'
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton'
import { red } from '@mui/material/colors';



function Header() {
    return (
      <div className='header-background'>
        <Box sx={{ flexGrow: 1}}>
          <AppBar position="static" sx={{backgroundColor:red}}>
            <Toolbar>
              <IconButton
                size="large"
                edge="start"
                
                
                sx={{mr: 2 }}
              />
            
            
              <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                tatil Seç
              </Typography>
              <h3>444 0 258</h3>
              <Button color="inherit">Giriş Yap</Button>
              <Button color="inherit">Kayıt Ol</Button>
            </Toolbar>
          </AppBar>
        </Box>
      </div>
    );
  }

  export default Header;