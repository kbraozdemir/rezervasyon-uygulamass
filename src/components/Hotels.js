import React, { Component } from 'react'
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';
import PropTypes from 'prop-types';
import Button from './Button';
import { red } from '@mui/material/colors';



class Hotels extends Component {
  
  state = {
    isVisible: false
  }

  onClickEvent= (e) => {

    this.setState({
      isVisible : !this.state.isVisible
    })

  }
  render() {

    
    const {name,price,facilities} = this.props;
    const {isVisible} = this.state;
    return (
      <Card sx={{ maxWidth: 345, backgroundColor: red }}>
      <CardActionArea>
        <CardContent>
          <Typography gutterBottom variant="h5" component="div" onClick = {this.onClickEvent}>
            {name}
          </Typography>
          <Typography variant="body2" color="text.secondary">
          {
            isVisible ? <div className='card-body'>
            <p className='card-text'>Fiyatı: {price}</p>
            <p className='card-text'>İmkanlar: {facilities}</p>
            
            </div> : null

            
          }
          <Button/>
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
    )
  }
}
Hotels.defaultProps = {
    name : "Bilgi Yok",
    price : "Bilgi Yok",
    facilities : "Bilgi Yok"
}

Hotels.propTypes = {
    
    name : PropTypes.string.isRequired,
    price : PropTypes.string.isRequired,
    facilities : PropTypes.string.isRequired 
}
export default Hotels;